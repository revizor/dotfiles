set -g default-terminal "screen-256color"

set-option -g prefix C-a
unbind-key C-b
bind-key a send-prefix
