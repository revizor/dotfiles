call pathogen#infect()
syntax on
filetype plugin indent on
set nocompatible
set number
set showcmd
"Tabstops

set tabstop=4
set shiftwidth=4
set expandtab
set backspace=indent,eol,start " backspace through everything in insert mode
set autoindent


set wildmode=list:longest
set foldenable

set t_Co=256
colorscheme xoria256
set guioptions-=T
set guioptions-=r
set guioptions=l
set guioptions-=l


"Font setting
set guifont=Inconsolata\ 12

"Window setting
"set lines=40 columns=90

"Search settings
set hlsearch "highlight matches
set incsearch "incremental serching
set ignorecase "searches are case insensitive
set smartcase "...unless thy contain at least one capital letter
set nowrap

"Navigation-defaults
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk
inoremap jj <ESC>

